package com.example.demo.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/students")
public class StudentController {

    private final StudentService studentService;

    //This is already Dependency Injection
        // Rather than stating new - we initialize this and flag it as a dependency
    @Autowired
    public StudentController(StudentService studentService){
        this.studentService = studentService;
    }
    //To get something out from the server
    //This annotation makes Rest Endpoint
    //The list will return the Hello World to a JSON format
    //This is going to create an array of object when creating an object
    @GetMapping
    public List<Student> getStudents(){
        return studentService.getStudents();
    }

    //We take the requestBody/Payload of the POST request and put it in student
    @PostMapping(path = "/mira")
    public void registerNewStudent(@RequestBody Student student){
        studentService.addNewStudent(student);
    }

    //This is for the delete request - the path seems to be the params
    @DeleteMapping(path = "{studentId}")
    public void deleteStudent(@PathVariable("studentId") Long studentId){
        studentService.deleteStudent(studentId);
    }

    @PutMapping(path = "{studentId}")
    public void updateStudent(
            @PathVariable("studentId") Long studentId,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String email){
        studentService.updateStudent(studentId, name, email);
    }
}
