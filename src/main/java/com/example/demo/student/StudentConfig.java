package com.example.demo.student;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository){
        return args -> {
            Student  mariam = new Student(
                    1L,
                    "Miriam",
                    LocalDate.of(2000, Month.FEBRUARY, 5),
                    "mariam.jamal@gmail.com"
            );

            Student  alex = new Student(
                    "Alex",
                    LocalDate.of(2004, Month.JANUARY, 5),
                    "alex.@gmail.com"
            );

            repository.saveAll(
                    List.of(mariam, alex)
            );
        };
    }

    /*
    @Bean
    @Qualifier("sec")
    public ScheduledExecutorService sec() {
        ScheduledExecutorService instance = new ScheduledThreadPoolExecutor()
                return instance;
    }
    */
}
