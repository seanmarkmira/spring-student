package com.example.demo.student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//Student Class where the object is being created and Long is the type of the ID
    //As you can see when you extend the JPARepository, it comes with T and Type as arguements -> where T is the class and Type is the datatype of the ID
//@Repository is used for the data access layer
@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    //Where Student is our Student class and from there we can query the properties of the student
    //Note: that this is not a SQL query
    @Query("SELECT s FROM Student s WHERE s.email = ?1")
    Optional<Student> findStudentByEmail(String email);
}
